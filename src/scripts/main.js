import app from './app/index.js';
import Accordion from '../components/accordion';
import Modal from '../components/modal';
import Carousel from '../components/carousel';
import Form from '../components/form';
import Menu from '../components/menu';

// register modules
app.registerModule(Accordion);
app.registerModule(Modal);
app.registerModule(Carousel);
app.registerModule(Form);
app.registerModule(Menu);
