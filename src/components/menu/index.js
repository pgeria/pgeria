import menuScene from './scenes/menuScene';

function Menu(state) {
  const { target } = state;
  const trigger =
    target.querySelector('.trigger') ||
    document.querySelector('.menu .trigger');
  const bodyElement = document.querySelector('body');

  const handleBody = (active) => {
    if (active) {
      bodyElement.style.position = '';
      bodyElement.style.overflow = '';
    } else {
      bodyElement.style.position = 'fixed';
      bodyElement.style.overflow = 'hidden';
    }
  };

  const menuHandler = () => {
    if (target.classList.contains('active')) {
      target.classList.remove('active');
      handleBody(true);
      menuScene(trigger.querySelector('.menu-icon'), true);
      return;
    }

    target.classList.add('active');
    handleBody(false);
    menuScene(trigger.querySelector('.menu-icon'), false);
    return;
  };

  trigger.addEventListener('click', menuHandler);
  trigger.addEventListener('touch', menuHandler);
}

export default Menu;
