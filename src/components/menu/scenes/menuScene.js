import velocity from 'velocity-animate';

const menuScene = (icon, isOpen) => {
  const top = icon.querySelector('.top');
  const middle = icon.querySelector('.middle');
  const bottom = icon.querySelector('.bottom');
  const timing = 350;

  if (isOpen) {
    velocity([top, middle, bottom, icon], 'reverse', timing);
  } else {
    velocity(icon, {
      rotateZ: '90deg',
    }, 350);
    velocity(top, {
      rotateZ: '45deg',
      x: '3px',
      y: '-3px',
      width: '28px',
      fill: '#ffffff',
    }, timing);
    velocity(middle, {
      opacity: 0,
      fill: '#ffffff',
    }, timing);
    velocity(bottom, {
      rotateZ: '-45deg',
      y: '15px',
      x: '-15px',
      width: '28px',
      fill: '#ffffff',
    }, timing);
  }
};

export default menuScene;
